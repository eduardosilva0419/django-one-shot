from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}

    return render(request, "lists.html", context)


def todo_list_detail(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    context = {"todo_lists": todo_lists}

    return render(request, "details.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "create.html", context)


def update_view(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_lists)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm(instance=todo_lists)

    context = {"form": form}

    return render(request, "edit.html", context)


def delete_view(request, id):
    todo_lists = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_lists.delete()
        return redirect("todo_list_list")

    context = {"todo_lists": todo_lists}

    return render(request, "delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save(commit=False)
            todo_item.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todo_item_create.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form = form.save(commit=False)
            form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {"form": form}
    return render(request, "todo_item_update.html", context)
